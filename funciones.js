function isNull(x){
    return x===null;
}
function isUndefined(x){    
    return x===undefined;
}
function isNotNull(x){
    return x !== null;
}
function isNotUndefined(x){
    return (x!==undefined);
}
function isTruty(x){
    return x ? true : false;
}
function isFalsy(x){
    return !x ? true : false;
}
function hasAValue(x){
    return x !== null && x !== undefined
}
function isNotNullOrUndefined(x){
    return x != null;
}

var primitiveBoolean=true;
var objectBolean=Boolean(true);
var primitiveString="123";
var objectString=String("123");
var objetito={}
var nullito;


//para volver a pasar a primitivo un objeto:
function toPrimitive(x){
        return x.valueOf();
}
function agregarPropaObjeto(){
    objetito['numero1']=2;
    console.log(objetito['numero1']);
}

function ocultarParrafo(){
   var parrafo= document.getElementById("oculto");
   parrafo.hidden=true;
}
function mostrarParrafo(){
    var parrafo= document.getElementById("oculto");
   parrafo.hidden=false;
}